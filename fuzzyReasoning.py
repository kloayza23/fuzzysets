from framework_fuzzy_logic import *

# 3 fuzzy set are created
print("-------------------------------------------------------------------")
print("-------------------Compositions of Fuzzy Relations------------------------")
print("-------------------------------------------------------------------")
print("Premises with one different variables")    
print("-------------------------------------------------------------------")
fsA1=[1.0,0.6,0.0]
fsA2=[0.0,0.8,1.0]
fsB1=[1.0,0.6,0.1]
fsB2=[0.2,0.8,0.9]
print("Fuzzy Set A1")
print(fsA1)
print("Fuzzy Set A2")
print(fsA2)
print("Fuzzy Set B1")
print(fsB1)
print("Fuzzy Set B2")
print(fsB2)
_rA1B1=convertRuleRelationTwoFuzzySets(fsA1,fsB1,1)
print("Relation between Fuzzy Set A1 and Fuzzy Set B1")
printRelationFuzzySet(_rA1B1)
_rA2B2=convertRuleRelationTwoFuzzySets(fsA2,fsB2,1)
print("Relation between Fuzzy Set A2 and Fuzzy Set B2")
printRelationFuzzySet(_rA2B2)
_uR1R2=unionFuzzyRelations(_rA1B1,_rA2B2,1)
print("Union between Relation 1(A1B1) and Relation 2(A2B2)")
printRelationFuzzySet(_uR1R2)
print("-------------------------------------------------------------------")
print("Premises with two different variables")
print("-------------------------------------------------------------------")
fsA1=[1.0,0.6,0.0]
fsA2=[0.0,0.8,1.0]
fsB1=[1.0,0.5,0.0]
fsB2=[0.0,0.2,0.9]
fsC1=[1.0,0.6,0.1]
fsC2=[0.2,0.8,0.9]

print("Fuzzy Set A1")
print(fsA1)
print("Fuzzy Set A2")
print(fsA2)
print("Fuzzy Set B1")
print(fsB1)
print("Fuzzy Set B2")
print(fsB2)
print("Fuzzy Set C1")
print(fsC1)
print("Fuzzy Set C2")
print(fsC2)

_rA1B1=convertRuleRelationThreeFuzzySets(fsA1,fsB1,fsC1,1)
_rA2B2=convertRuleRelationThreeFuzzySets(fsA2,fsB2,fsC2,1)
_unionFuzzyRel=[]
for i in range(3):
	_unionFuzzyRel.append(unionFuzzyRelations(_rA1B1[i],_rA2B2[i],1))

print("Relation 1: A1 and B1 => C1")
for i in range(3):
	print("---------------")
	printRelationFuzzySet(_rA1B1[i])

print("Relation 2: A2 and B2 => C2")
for i in range(3):
	print("---------------")
	printRelationFuzzySet(_rA2B2[i])

print("Union between Ralation 1 and 2")
for i in range(3):
	print("---------------")
	printRelationFuzzySet(_unionFuzzyRel[i])

print("-------------------------------------------------------------------")
print("Compositional Rule of Inference")    
print("-------------------------------------------------------------------")
print("Case 1")
print("-------------------------------------------------------------------")
Ap=[0.8,0.3,0.0]
print("Fuzzy Set A:")
print(Ap)
Bp=compositionFuzzySetWithRelation(Ap,_uR1R2)
print("Fuzzy Set B prime composed:")
print(Bp)
print("-------------------------------------------------------------------")
print("Case 2")
print("-------------------------------------------------------------------")
Ap=[0.8,0.3,0.0]
Bp=[0.4,0.0,0.9]
print("Given two fuzzy sets:")
print("Fuzzy Set A':")
print(Ap)
print("Fuzzy Set B':")
print(Bp)
print("The relation between A and B")
for i in range(3):
	print("-------------------------")
	printRelationFuzzySet(_unionFuzzyRel[i])
print("-----------------------------------")
print("Now is necessary to find a relation between A' with R")
_tempCompositionalRel=[]
for i in range(3):
	_tempCompositionalRel.append(compositionFuzzySetWithRelation(Ap,_unionFuzzyRel[i]))
print("Temporal relation A' o R")
printRelationFuzzySet(_tempCompositionalRel)
print("-----------------------------------")
_tempCompositionalRel=np.array(_tempCompositionalRel).transpose().tolist()
printRelationFuzzySet(_tempCompositionalRel)
print("With this relation is possible to find the conclusion thorugh B' with this temporal relation")
Cp=compositionFuzzySetWithRelation(Bp,_tempCompositionalRel)
print("-------------------------------------------------------------------")
print("The conclusion is:")
print(Cp)




















