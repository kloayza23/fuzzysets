from framework_fuzzy_logic import *
if __name__ == '__main__':
    print("-------------------------------------------------------------------")
    print("-------------------Guessing Game------------------------")
    print("-------------------------------------------------------------------")
    print("Each fruit have the following shape: [long, round, large]")
    fuzzyRealtionFruitShape=[]
    tangerine=[0.0,0.9,0.2]
    apple=[0.0,1.0,0.3]
    pineapple=[0.3,0.3,0.7]
    watermelon=[0.0,1.0,1.0]
    strawberry=[0.8,0.2,0.1]
    fuzzyRealtionFruitShape.append(tangerine)
    fuzzyRealtionFruitShape.append(apple)
    fuzzyRealtionFruitShape.append(pineapple)
    fuzzyRealtionFruitShape.append(watermelon)
    fuzzyRealtionFruitShape.append(strawberry)
    print("Tangerine:", end='')
    print(tangerine)
    print("apple:", end='')
    print(apple)
    print("pineapple:", end='')
    print(pineapple)
    print("watermelon:", end='')
    print(watermelon)
    print("strawberry:", end='')
    print(strawberry)
    print("------------------------------")
    print("Given the relation, where is match the shape of the fruit with their name")
    print("------------------------------")
    fuzzyRealtionFruitShape=np.array(fuzzyRealtionFruitShape).transpose().tolist()
    printRelationFuzzySet(fuzzyRealtionFruitShape)
    print("------------------------------")
    print("The goal is that given the shape of a fruit, this should to guess what fruit is fit to this shape.")
    example=[0.0,0.7,1.0]
    print("The shape of the fruit is")
    print(example)
    print("The example of shape is:")
    guessFruit=compositionFuzzySetWithRelation(example,fuzzyRealtionFruitShape)
    indexGuess=guessFruit.index(max(guessFruit))
    print(guessFruit)
    fruits=['tangerine','apple','pineapple','watermelon','strawberry']
    print("the fruit with the shape fittest is ",end=': ')
    print(fruits[indexGuess])
