from framework_fuzzy_logic import *
#import plotly.plotly as pyplot
#import matplotlib.pyplot as plt
#plt.style.use('seaborn-whitegrid')
if __name__ == '__main__':
    fa= [0,0.1,0.6,1,0.6,0.1,0,0,0,0]
    fb=[0,0,0.1,0.4,0.7,1,0.7,0.4,0.1,0]
    print("Fuzzy set A")
    print(fa)
    print("-------------------------------------------------------------------")
    print("Fuzzy set B")
    print(fb)
    arr_union=unionFuzzySet(fa,fb)
    arr_inter=intersectionFuzzySet(fa,fb)
    x=[0.4,0.3,0.5]
    y=[0.2,0.1,0.9]
    arr_fuzzy_relation_R=[[1.0,0.8,0.4],[0.7,0.8,0.5],[0.1,0.5,0.9]]
    arr_fuzzy_relation_S=[[0.2,0.1,0.6],[0.9,0.8,0.5],[0.3,0.2,1.0]]
    print("-------------------------------------------------------------------")
    print("Union Fuzzy Set A and B")
    print(arr_union)
    print("-------------------------------------------------------------------")
    print("Intersection")
    print(arr_inter)
    alphas=[0.2,0.6,0.8]
    arrays_alpha=alphacuts(fa,alphas)
    print("Alpha cuts")
    print("-------------------------------------------------------------------")
    print("Alpha cut 0.2:")
    print(arrays_alpha[0])
    print("-------------------------------------------------------------------")
    print("Alpha cut 0.6:")
    print(arrays_alpha[1])
    print("-------------------------------------------------------------------")
    print("Alpha cut 0.8:")
    print(arrays_alpha[2])
    print("-------------------------------------------------------------------")
    print("fuzzy relation R")
    print("                   ")
    printRelationFuzzySet(arr_fuzzy_relation_R)
    print("-------------------------------------------------------------------")
    print("fuzzy relation S")
    print("                   ")
    printRelationFuzzySet(arr_fuzzy_relation_S)
    print("-------------------------------------------------------------------")
    fuzzy_relation_union=unionFuzzyRelations(arr_fuzzy_relation_R,arr_fuzzy_relation_S,1)# 1 mean union
    print("Union between fuzzy relation R and fuzzy relation S")
    print("                   ")
    printRelationFuzzySet(fuzzy_relation_union)
    print("-------------------------------------------------------------------")
    fuzzy_relation_inter=unionFuzzyRelations(arr_fuzzy_relation_R,arr_fuzzy_relation_S,2)# 1 mean intersection
    print("Intersection between fuzzy relation R and fuzzy relation S")
    print("                   ")
    printRelationFuzzySet(fuzzy_relation_inter)
    print("-------------------------------------------------------------------")
    print("Compositions between a fuzzy set and a fuzzy relation.")
    fuzzy_A= [0.2,1.0,0.5]
    fuzzyRelation=[[0.8,0.9,0.1],[0.6,1.0,0.5],[0.2,0.3,0.9]]
    print("")
    print("fuzzy set A")
    print(fuzzy_A)
    print("------------------------------")
    print("fuzzy relation R")
    printRelationFuzzySet(fuzzyRelation)
    print("------------------------------")
    composiFuzzy=compositionFuzzySetWithRelation(fuzzy_A,fuzzyRelation)
    print("Composition between fuzzy set A and the fuzzy relation R")
    print(composiFuzzy)
    print("-------------------------------------------------------------------")
    print("Compositions between a two Fuzzy Relations")
    fuzzyRelationA=[[1.0,0.6,0.3],[0.4,0.9,0.1],[0.5,0.2,0.7]]
    fuzzyRelationB=[[1.0,0.1,0.5],[0.7,0.9,0.2],[0.1,0.8,0.8]]
    print("------------------------------")
    print("fuzzy relation R")
    printRelationFuzzySet(fuzzyRelationA)
    print("------------------------------")
    print("fuzzy relation S")
    printRelationFuzzySet(fuzzyRelationB)
    print("------------------------------")
    composiFuzzy=compositionBetweenFuzzyRelations(fuzzyRelationA,fuzzyRelationB)
    print("Composition between two fuzzy relations RoS")    
    printRelationFuzzySet(composiFuzzy)
    # plt.plot(fa)
    # plt.plot(fb)
    # plt.plot(arr_inter)
    # plt.ylabel('Fuzzy Sets')
    # plt.show()
