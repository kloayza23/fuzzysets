from framework_fuzzy_logic import *
if __name__ == '__main__':
    print("-------------------------------------------------------------------")
    print("-------------------MatchMaking------------------------")
    print("-------------------------------------------------------------------")
    print("The goal is to find what candidate is fittest to buy a house, but only the price car is get as information, but this price has the way of fuzzy number")
    print("The shape of the price car is [cheap, middle, expensive]")
    CarB=[0.9,0.1,0.0]
    print("The fuzzy price of B candidate: ", end=":")
    print(CarB)
    CarC=[0,0.2,0.8]
    print("The fuzzy price of C candidate: ", end=":")
    print(CarC)
    CarD=[0.3,0.6,0.1]
    print("The fuzzy price of D candidate: ", end=":")
    print(CarD)
    carPrice=['cheap','middle','expensive']
    income=['low','average','high']
    mortgage=['short','average','long']
    print("Exist the following relations")
    print("Relation between Price Car fuzzy and Income fuzzy")
    carIncomeRel=[[0.6,0.3,0.3],[0.3,0.8,0.3],[0.1,0.3,0.7]]
    printRelationFuzzySet(carIncomeRel)
    print("Relation between Income fuzzy and Mortage fuzzy")
    incomeMortageRel=[[0.0,0.3,1.0],[0.2,0.6,0.5],[0.8,0.4,0.4]]
    printRelationFuzzySet(incomeMortageRel)
    print("The fuzzy relation obtained by the composition of the previous relations give a fuzzy relation between Price car and Mortage Fuzzy")
    carMortageRel=compositionBetweenFuzzyRelations(carIncomeRel,incomeMortageRel)
    printRelationFuzzySet(carMortageRel)
    print("Comparing each Price car with this relation, so the outcome is the candidate with the payment capacity more suitable")
    Bmortage=compositionFuzzySetWithRelation(CarB,carMortageRel)
    print("The probability of pay the B candidate is: "+str(Bmortage[0]*100)+"%")
    Cmortage=compositionFuzzySetWithRelation(CarC,carMortageRel)
    print("The probability of pay the C candidate is: "+str(Cmortage[0]*100)+"%")
    Dmortage=compositionFuzzySetWithRelation(CarD,carMortageRel)
    print("The probability of pay the D candidate is: "+str(Dmortage[0]*100)+"%")
    print("The candidate more suitable to buy a house is the C")
