import numpy as np
def unionFuzzySet(arra,arrb):
    arraub=[]
    for i in range(len(arra)):
        arraub.append(max(arra[i],arrb[i]))
    return arraub
    # 1 Mamdani
    # 2 Zadeh
def unionFuzzyRelations(arra,arrb,type):
    array_union_rela=[]
    for i in range(len(arra)):
        array_ur_row=[]
        for j in range(len(arra[0])):
            if(type==1):
                array_ur_row.append(max(arra[i][j],arrb[i][j]))
            elif(type==2):
                array_ur_row.append(min(arra[i][j],arrb[i][j]))
        array_union_rela.append(array_ur_row)
    return array_union_rela
    # 1 Mamdani
    # 2 Zadeh
def convertRuleRelationTwoFuzzySets(fsA,fsB,kind):
    _outRel=[]
    for item_fa in fsA:
        _rowRel=[]
        for item_fb in fsB:
            if(kind==1):
                _rowRel.append(min(item_fa,item_fb))
        _outRel.append(_rowRel)
    return _outRel
    # 1 Mamdani
    # 2 Zadeh
def convertRuleRelationThreeFuzzySets(fsA,fsB,fsC,kind):
    _outRel=[]
    for item_fc in fsC:
        _matrixRel=[]
        for item_fa in fsA:
            _rowRel=[]
            for item_fb in fsB:
                if(kind==1):
                    _rowRel.append(min(item_fa,item_fb,item_fc))
            _matrixRel.append(_rowRel)
        _outRel.append(_matrixRel)
    return _outRel
def centerGravity(fuzzySet,fuzzyExa):
    sumTot=0
    cGra=0
    for i in range(len(fuzzySet)):
        sumTot+=fuzzySet[i]*fuzzyExa[i]
    if sum(fuzzySet)!=0:
        cGra=sumTot/sum(fuzzySet)
    else:
        cGra=0
    return cGra
def compositionFuzzySetWithRelation(fuzzySet,fuzzyRelation):
    fuzzyRel=np.array(fuzzyRelation).transpose().tolist()
    array_compos=[]
    for columnRe in fuzzyRel:
        row_c=[]
        for item in range(len(columnRe)):
            row_c.append(min(columnRe[item],fuzzySet[item]))
        val_compos=np.amax(np.array(row_c))
        array_compos.append(val_compos)
    return array_compos
def compositionBetweenFuzzyRelations(fuzRelA,fuzRelB):
    outRelation=[]
    for rowA in fuzRelA:
        outRelation.append(compositionFuzzySetWithRelation(rowA,fuzRelB))
    return outRelation
def printRelationFuzzySet(relationFuzzy):
    for re_row in relationFuzzy:
        print("|", end="")
        for item in re_row:
            print(item, end='|')
        print("")
def alphacuts(arra,alphas):
    compl_arr_alp=[]
    for alpha in alphas:
        arrays_alpha=[]
        for item in arra:
            if(alpha<item):
                arrays_alpha.append(1)
            else:
                arrays_alpha.append(0)
        compl_arr_alp.append(arrays_alpha)
    return compl_arr_alp
def addFuzzyNumber(fuzzy_a,fuzzy_b):
    return [fuzzy_a[0]+fuzzy_b[0],fuzzy_a[1]+fuzzy_b[1]]
def subsFuzzyNumber(fuzzy_a,fuzzy_b):
    return [fuzzy_a[0]-fuzzy_b[1],fuzzy_a[1]-fuzzy_b[0]]
def multFuzzyNumber(fuzzy_a,fuzzy_b):
    return [fuzzy_a[0]*fuzzy_b[0],fuzzy_a[1]*fuzzy_b[1]]
def divFuzzyNumber(fuzzy_a,fuzzy_b):
    return [fuzzy_a[0]/fuzzy_b[1],fuzzy_a[1]/fuzzy_b[0]]
def intersectionFuzzySet(arra,arrb):
    arraub=[]
    for i in range(len(arra)):
        arraub.append(min(arra[i],arrb[i]))
    return arraub
