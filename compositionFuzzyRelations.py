from framework_fuzzy_logic import *
print("-------------------------------------------------------------------")
print("---------Example of Car' Actions---------")
print("-------------------------------------------------------------------")
fsA1=[1.0,0.5,0.0]
fsA2=[0.0,0.5,1.0]
fsB1=[1.0,0.5,0.0]
fsB2=[0.0,0.5,1.0]
fsC1=[0.0,1.0,0.0]
fsC2=[1.0,0.0,0.0]
fsC3=[0.0,0.0,1.0]
print("-------------------------------------------------------------------")
print("There are two premises of distance and two premises of speed:")
print("First premise of distance:")
print(fsA1)
print("Second premise of distance:")
print(fsA2)
print("First premise of speed:")
print(fsB1)
print("Second premise of speed:")
print(fsB2)
print("-------------------------------------------------------------------")
_R1=convertRuleRelationThreeFuzzySets(fsA1,fsB1,fsC1,1)
_R2=convertRuleRelationThreeFuzzySets(fsA1,fsB2,fsC2,1)
_R3=convertRuleRelationThreeFuzzySets(fsA2,fsB1,fsC3,1)
_R4=convertRuleRelationThreeFuzzySets(fsA2,fsB2,fsC1,1)

print("There 4 rules based in the previous premises, the 4 rules have been convert to reations through of the fuzzy reasoning:")
print("Relation1: ")
for i in range(3):
	print("---------------------")
	printRelationFuzzySet(_R1[i])
print("------------------------------------------------------------------------------------")
print("Relation2: ")
for i in range(3):
	print("---------------------")
	printRelationFuzzySet(_R2[i])
print("------------------------------------------------------------------------------------")
print("Relation3: ")
for i in range(3):
	print("---------------------")
	printRelationFuzzySet(_R3[i])
print("------------------------------------------------------------------------------------")
print("Relation4: ")
for i in range(3):
	print("---------------------")
	printRelationFuzzySet(_R4[i])
print("------------------------------------------------------------------------------------")

TR12=[]
for i in range(3):
	TR12.append(unionFuzzyRelations(_R1[i],_R2[i],1))


TR34=[]
for i in range(3):
	TR34.append(unionFuzzyRelations(_R3[i],_R4[i],1))

rTotal=[]
for i in range(3):
	rTotal.append(unionFuzzyRelations(TR12[i],TR34[i],1))

print("The 4 rules have been join in one only relation thorugh the fuzzy union function")
print("The Fuzzy Relation unified:")
for i in range(3):
	print("---------------------")
	printRelationFuzzySet(rTotal[i])


print("-------------------------------------------------------------------")
print("-------Reasoning by fuzzy relations and compositional rule---------")
print("-------------------------------------------------------------------")

Ap=[1.0,0.0,0.0] # distance
Bp=[1.0,0.0,0.0] # speed

print("To test the relation is need two examples, one of distance and another speed, both fuzzy")
print("Fuzzy distance:")
print(Ap)
print("Fuzzy Speed:")
print(Bp)
print("The conclusion of three possibily:")
print("Go ahead, Constante acceleration, Brake")
print("To find out the conclusion is needed to applied the following formula:")
print("C' = B' o (A' o R)")
print("R is the unified relation computed previously, A belong to distance and B to speed")
print("In order to compute A' o R, is compused in a fuzzy temporal relation T = A' o R")
_tempCompositionalRel=[]
for i in range(3):
	_tempCompositionalRel.append(compositionFuzzySetWithRelation(Ap,rTotal[i]))
_tempCompositionalRel=np.array(_tempCompositionalRel).transpose().tolist()

print("T = A' o R:")
printRelationFuzzySet(_tempCompositionalRel)
Cp=compositionFuzzySetWithRelation(Bp,_tempCompositionalRel)
print("-------------------------------------------------------------------")
print("The conclusion C= B' o T:")
print(Cp)

print("To test this conclusion, a example has been created")
fuzzyExample=[-10,1,10]
print("Fuzzy example of the conclusion is:")
print(fuzzyExample)
cG=centerGravity(Cp,fuzzyExample)
print(" The gravity center of this example is:")
print(cG)





